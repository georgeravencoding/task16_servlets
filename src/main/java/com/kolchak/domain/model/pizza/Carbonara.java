package com.kolchak.domain.model.pizza;

public class Carbonara extends Pizza {

    public Carbonara(String name, int price) {
        super(name, price);
    }
}
