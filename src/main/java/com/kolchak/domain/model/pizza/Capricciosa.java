package com.kolchak.domain.model.pizza;

public class Capricciosa extends Pizza {
    public Capricciosa(String name, int price) {
        super(name, price);
    }
}
