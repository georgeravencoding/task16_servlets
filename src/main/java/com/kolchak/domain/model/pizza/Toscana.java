package com.kolchak.domain.model.pizza;

public class Toscana extends Pizza {

    public Toscana(String name, int price) {
        super(name, price);
    }
}
