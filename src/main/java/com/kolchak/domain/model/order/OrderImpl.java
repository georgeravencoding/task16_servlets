package com.kolchak.domain.model.order;

import com.kolchak.domain.model.delivery.Delivery;
import com.kolchak.domain.model.pizza.Pizza;

public class OrderImpl {
    private static int unique = 1;
    private int orderNum;
    private Pizza pizza;
    private Delivery delivery;
    private int totalPrice;

    public OrderImpl(Pizza pizza, Delivery delivery) {
        this.pizza = pizza;
        this.delivery = delivery;
        orderNum = unique;
        unique++;
        this.totalPrice = pizza.getPrice() + delivery.showDeliveryPrice();
    }


    public int getOrderNum() {
        return orderNum;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    @Override
    public String toString() {
        return "\nOrder: " + orderNum
                + "<br>"
                + "\nPizza: " + pizza.getName()
                + "<br>"
                + "\nDelivery type: " + delivery.getClass().getSimpleName().toLowerCase()
                + "<br>"
                + "\nTotal Price: " + totalPrice
                + "<br>"
                + "\n-------------------------";
    }

}
