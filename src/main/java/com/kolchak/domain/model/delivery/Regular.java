package com.kolchak.domain.model.delivery;

public class Regular implements Delivery {
    private static int regular_price = 10;


    public static int getRegular_price() {
        return regular_price;
    }

    @Override
    public int showDeliveryPrice() {
        return regular_price;
    }
}
