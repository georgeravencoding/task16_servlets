package com.kolchak.domain.model.delivery;

public interface Delivery {
    int showDeliveryPrice();
}
