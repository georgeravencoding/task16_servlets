package com.kolchak.domain.model.delivery;

public class Express implements Delivery {
    private static int express_price = 20;

    public static int getExpress_price() {
        return express_price;
    }

    @Override
    public int showDeliveryPrice() {
        return express_price;
    }
}
