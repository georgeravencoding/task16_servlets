package com.kolchak.domain.controller;

import com.kolchak.domain.model.delivery.Delivery;
import com.kolchak.domain.model.delivery.Express;
import com.kolchak.domain.model.delivery.Regular;
import com.kolchak.domain.model.order.OrderImpl;
import com.kolchak.domain.model.pizza.Capricciosa;
import com.kolchak.domain.model.pizza.Carbonara;
import com.kolchak.domain.model.pizza.Pizza;
import com.kolchak.domain.model.pizza.Toscana;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/orders/*")
public class OrderServlet extends HttpServlet {
    private static Logger logger = LogManager.getLogger(OrderServlet.class);
    private static Map<Integer, OrderImpl> orders = new HashMap<>();

    @Override
    public void init() throws ServletException {
        logger.info("Lets get an order.");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info(this.getServletName() + " into doGet()");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<h3> <a href='orders'>Refresh page</a></h3>");

        out.println("<h2>Order list</h2>");
        for (OrderImpl order : orders.values()) {
            out.println("<p>" + order + "</p>");
        }

        out.println("<form action='orders' method='POST'>\n"
                + "<p>Choose your pizza:</p>"
                + "<input type='radio' name='pizza' value='carbonara' checked> Carbonara"
                + "<input type='radio' name='pizza' value='toscana'> Toscana"
                + "<input type='radio' name='pizza' value='toscana'> Capricciosa"
                + "<p>Choose your delivery:</p>"
                + "<input type='radio' name='delivery' value='express' checked> Express"
                + "<input type='radio' name='delivery' value='regular'> Regular"
                + "<br>"
                + "<br>"
                + "<button type='submit'>Confirm order</button>\n"
                + "</form>");

        out.println("<form>\n"
                + "  <p><b>Cancel order</b></p>\n"
                + "  <p> Order number: <input type='text' name='order_id'>\n"
                + "    <input type='button' onclick='remove(this.form.order_id.value)' name='ok' value='Cancel order'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('orders/' + id, {method: 'DELETE'}); }\n"
                + "</script>");

        out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger.info("----------doPost------------");

        String newPizza = req.getParameter("pizza");
        String newDelivery = req.getParameter("delivery");
        Pizza pizza = null;
        Delivery delivery = null;

        if (newPizza.equals("carbonara")) {
            pizza = new Carbonara("carbonara", 35);
        } else if (newPizza.equals("toscana")) {
            pizza = new Toscana("toscana", 20);
        } else if (newDelivery.equals("capricciosa")) {
            pizza = new Capricciosa("capricciosa",50);
        }

        if (newDelivery.equals("express")) {
            delivery = new Express();
        } else if (newDelivery.equals("regular")) {
            delivery = new Regular();
        }
        OrderImpl newOrder = new OrderImpl(pizza, delivery);
        orders.put(newOrder.getOrderNum(), newOrder);
        doGet(req, resp);
    }

    @Override
    public void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger.info("----------doDelete------------");
        String orderNum = req.getRequestURI();
        logger.info("URI=" + orderNum);
        orderNum = orderNum.replace("/kolchak-servlets/orders/", "");
        logger.info("Order: " + orderNum);
        orders.remove(Integer.parseInt(orderNum));
    }
//    @Override
//    public void destroy() {
//        logger.info("Servlet " + this.getServletName() + " has stopped");
//    }
}
